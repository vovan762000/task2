/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jv21701.bondarenko.task2;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author vovan
 */
public class App {

    private static final String URL = "jdbc:mysql://localhost:3306/jdbc_example";
    private static final String USERNAME = "root";
    private static final String PASSWORD = "user";

    static int rowCount(ResultSet resultSet) throws SQLException {
        resultSet.beforeFirst();
        resultSet.last();
        int rowCount = resultSet.getRow();
        resultSet.beforeFirst();

        return rowCount;
    }

    static String[] selectFromTable(Statement statement, String select) throws SQLException {

        int arrayIndex = 0;
        ResultSet resultSet = statement.executeQuery(select);
        int columns = resultSet.getMetaData().getColumnCount();

        int rows = rowCount(resultSet);

        String[] values = new String[columns * rows];

        while (resultSet.next()) {
            for (int column = 1; column < columns + 1; column++) {
                values[arrayIndex++] = resultSet.getString(column);
            }
        }
        return values;
    }

    static User[] returnUsers(Statement statement, String select) throws SQLException {

        int arrayIndex = 0;
        ResultSet resultSet = statement.executeQuery(select);

        int rows = rowCount(resultSet);

        User[] users = new User[rows];

        while (resultSet.next()) {
            users[arrayIndex++] = new User(resultSet.getInt(1),
                    resultSet.getString(2),
                    resultSet.getString(3),
                    resultSet.getString(4));
        }
        return users;
    }

    static <T> T[] returnUsersWithRefl(Statement statement, String select, String nameClass) throws SQLException,
            ReflectiveOperationException {

        int arrayIndex = 0;
        ResultSet resultSet = statement.executeQuery(select);
        int rows = rowCount(resultSet);

        Class someClass = Class.forName(nameClass);
        T[] someObjecTs = (T[]) Array.newInstance(someClass, rows);

        Field[] fields = someClass.getDeclaredFields();
        Method[] methods = someClass.getMethods();

        int columnsVal = resultSet.getMetaData().getColumnCount();
        String[] values = new String[columnsVal];

        for (int column = 1; column < columnsVal + 1; column++) {
            values[arrayIndex++] = resultSet.getMetaData().getColumnName(column);
        }

        arrayIndex = 0;

        while (resultSet.next()) {

            someObjecTs[arrayIndex] = (T) someClass.newInstance();

            for (int i = 0; i < values.length; i++) {
                for (int j = 0; j < fields.length; j++) {
                    if (values[i].equalsIgnoreCase(fields[j].getName())) {
                        for (int k = 0; k < methods.length; k++) {
                            if (methods[k].getName().equalsIgnoreCase("set" + values[i])) {
                                Class[] paramTypes = methods[k].getParameterTypes();
                                Method method = someClass.getMethod(methods[k].getName(), paramTypes);
                                Object[] args = new Object[]{resultSet.getObject(fields[j].getName())};
                                method.invoke(someObjecTs[arrayIndex], args);
                            }
                        }
                    }
                }
            }
            arrayIndex++;
        }
        return someObjecTs;

    }

    public static void main(String[] args) {
        try (Connection connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
                Statement statment = connection.createStatement()) {

            String[] result = selectFromTable(statment, "select*from users where id=3");
            for (String el : result) {
                System.out.println(el);
            }
            System.out.println("-------------------------------------------------------");
            User[] users = returnUsers(statment, "select*from users");
            for (User user : users) {
                System.out.println(user);
            }
            System.out.println("---------------------------------------------------------");
            User[] usersWithRefl = returnUsersWithRefl(statment, "select id,email from users", "jv21701.bondarenko.task2.User");
            for (User returnUser : usersWithRefl) {
                System.out.println(returnUser);
            }

        } catch (SQLException | ReflectiveOperationException e) {
            System.out.println("SQL or ReflectiveOperation Exception");
        }
    }
}
